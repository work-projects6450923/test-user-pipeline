from kfp.v2 import dsl, compiler
from kfp.v2.google.client import AIPlatformClient

import json
import os

ENV = os.getenv("ENV")
OPERATION = os.getenv("OPERATION")

BDF_PROJECT = f'gcp-ent-business-fndn-{ENV}'
CALLING_PROJECT = f'gcp-cah-datascience-{ENV}'

SQL = """
SELECT
  DISTINCT ent_cust_id
FROM
  `gcp-ent-datalake-prod.pb_ecp_ent_reporting_visible.vw_ecp_unified_report` ur
WHERE
  UPPER(IFNULL(ur.prim_nm_insd_first_nm,'TEST')) IN ('TEST',
    'FIRSTNAMETEST',
    'TESTFN',
    'FNTEST')
  AND NOT EXISTS (
  SELECT
    *
  FROM
    `gcp-ent-datalake-prod.pb_ecp_ent_reporting_visible.vw_ecp_unified_report` ur2
  WHERE
    ur2.ent_cust_id = ur.ent_cust_id
    AND UPPER(IFNULL(ur2.prim_nm_insd_first_nm,'TEST')) NOT IN ('TEST',
      'FIRSTNAMETEST',
      'TESTFN',
      'FNTEST'))
"""

DATASET = "pb_ecp_ent"


@dsl.component(
    base_image="gcr.io/deeplearning-platform-release/base-cpu",
    packages_to_install=["google-cloud-bigquery"],
)
def create_tables_stage(calling_project: str, bdf_project: str, sql: str, bdf_dataset: str):
    
    from google.cloud import bigquery
    from google.cloud.exceptions import NotFound

    import pandas as pd
    import numpy as np

    def create_dataset(calling_project: str, bdf_project: str, dataset_id: str):
        client = bigquery.Client(calling_project)
        dataset_ref = bigquery.DatasetReference(bdf_project, dataset_id)
        client.create_dataset(dataset_ref)
    
    def query_original_table(sql: str, project_id: str):
        bqclient = bigquery.Client(project_id)
        return bqclient.query(sql).result().to_dataframe()

    def upload_df(df: pd.DataFrame, calling_project: str, bdf_project: str, dataset_id: str, table_id: str):
        client = bigquery.Client(calling_project)
        dataset_ref = bigquery.DatasetReference(bdf_project, dataset_id)

        job_config = bigquery.job.LoadJobConfig()
        job_config.write_disposition = bigquery.WriteDisposition.WRITE_TRUNCATE

        job = client.load_table_from_dataframe(
            df,
            bigquery.TableReference(dataset_ref, table_id),
            job_config = job_config
        )
        job.result()

    try:
        client = bigquery.Client(calling_project)
        client.get_dataset(bigquery.DatasetReference(bdf_project, bdf_dataset))
    except NotFound:
        create_dataset(calling_project, bdf_project, bdf_dataset)
    
    df = query_original_table(sql, calling_project)
    upload_df(
        df, 
        calling_project, 
        bdf_project, 
        bdf_dataset,
        "invalid_ent_cust_ids"
    )

@dsl.component(
    base_image="gcr.io/deeplearning-platform-release/base-cpu",
    packages_to_install=["google-cloud-bigquery"],
)
def delete_dataset_stage(calling_project: str, bdf_project: str, bdf_dataset: str):
    from google.cloud import bigquery
    import pandas as pd
    import numpy as np

    def delete_dataset(calling_project: str, bdf_project: str, dataset_id: str):
        client = bigquery.Client(calling_project)
        dataset_ref = bigquery.DatasetReference(bdf_project, dataset_id)
        client.delete_dataset(dataset_ref, delete_contents=True, not_found_ok=True)
    
    delete_dataset(calling_project, bdf_project, bdf_dataset)

@dsl.pipeline(
    pipeline_root=f"gs://connect-bdf-pipeline-{ENV}",
    name="bdf-pb-ecp-ent"
)
def pipeline():
    if OPERATION == 'create':
        create_tables_stage(CALLING_PROJECT, BDF_PROJECT, SQL, DATASET)
    elif OPERATION == 'delete':
        delete_dataset_stage(CALLING_PROJECT, BDF_PROJECT, DATASET)
    else:
        raise Exception('unknown operation')

if __name__ == "__main__":
    compiler.Compiler().compile(
        package_path='pb_ecp_ent.json',
        pipeline_func=pipeline,
    )

    api_client = AIPlatformClient(project_id=f"gcp-cah-datascience-{ENV}", region="us-central1")

    response = api_client.create_run_from_job_spec(
        'pb_ecp_ent.json',
        enable_caching=False,
        service_account=f"vertex-pipeline-sa@gcp-cah-datascience-{ENV}.iam.gserviceaccount.com"
    )